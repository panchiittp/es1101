import sys

def WriteHeaderonFile(writefile,readfile):
    with open(readfile) as f:
        with open(writefile, "w") as g:
            for line in f:
                g.write(line)

def WriteonFile(writefile,readfile):
    with open(readfile) as f:
        with open(writefile, "a") as g:
            for line in f:
                g.write(line) 

def WriteTextonFile(writefile,text):
    with open(writefile, "a") as g:           
         g.write(text) 
         g.write("\n")

def WriteTextonFileFirst(writefile,text):
    with open(writefile, "w") as g:           
         g.write(text) 
         g.write("\n")


FileName='ES1101.html'
DirName='HTML/'
HTMLFiles=['Title','MarksRef','ComputerSystem','History',
'HardwareSoftware','ComputerOrganization','MLALHLL','CProgram',
'CStandard','StandarCLibrary','OtherLanguage','CProgramDevelopment']
WriteHeaderonFile(FileName,'HTML/HeadPage.html')
for i in range(len(HTMLFiles)):
    HTMLFileName=DirName+HTMLFiles[i]+'.html'
    WriteonFile(FileName,HTMLFileName)
WriteonFile(FileName,'HTML/EndPage.html')

